<div class="row justify-content-center">
    <div class="col-lg-8 col-md-10 col-sm-10">
        <div class="bg0 how-pos3-parent">
            <button class="how-pos3 hov3 trans-04 js-hide-modal1">
                <img src="{{ asset('/front') }}/images/icons/icon-close.png" alt="CLOSE">
            </button>

            <div class="row">
                <form action="{{ route('update_profile', Auth::user()->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-3 col-lg-5 col-sm-4">
                        <div class="wrap-slick3-dots"></div>
                        <div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

                        <div class="gallery-lb">
                            <div class="wrap-pic-w pos-relative">
                                @if(Auth::user()->photo_profile)
                                <img src="{{ url('/storage/'. Auth::user()->photo_profile) }}" id="preview_photo" alt="IMG-PRODUCT">
                                <a class="flex-c-m size-108 m-l-20 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04 zoom-picture" href="{{ url('/storage/'. Auth::user()->photo_profile)}}">
                                    <i class="fa fa-expand"></i>
                                </a>
                                @else
                                <img src="https://ui-avatars.com/api/?name={{ Auth::user()->name }}" id="preview_photo" alt="IMG-PRODUCT">
                                @endif
                                <input type="hidden" id="old_photo_profile" value="{{ Auth::user()->photo_profile }}">
                                <input class="stext-111 cl2 plh3 p-l-62 p-r-30" id="photo" type="file" name="photo" style="opacity: 0; height:1px; display:none;" onchange="previewImage(this)">
                                <button onclick="window.location.href='javascript:void(0)'" class="changePhoto" id="change_picture">Ganti Foto</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-lg-7 col-sm-8 p-b-30">
                        <div class="p-r-50 p-t-30 p-lr-0-lg">
                            <label for="">Nama</label>
                            <input type="hidden" name="" id="id_edit" value="{{ Auth::user()->id }}">
                            <div class="bor8 m-b-20 how-pos4-parent">
                                <input class="stext-111 cl2 plh3 size-126 p-l-62 p-r-30" id="nama_edit" type="text" name="name" placeholder="Your Username" value="{{ Auth::user()->name }}">
                                <i class="how-pos4 pointer-none fa fa-user-o"></i>
                            </div>
                            <p class="text-danger m-b-20" style="margin-top: -20px;" id="nama_e"></p>
                            <label for="">Email</label>
                            <div class="bor8 m-b-20 how-pos4-parent w-100">
                                <input class="stext-111 cl2 plh3 size-126 p-l-62 p-r-30" id="email_edit" type="email" name="email" placeholder="Your Email Address" value="{{ Auth::user()->email }}">
                                <i class="how-pos4 pointer-none fa fa-envelope-o"></i>
                            </div>
                            <p class="text-danger m-b-20" style="margin-top: -20px;" id="email_e"></p>
                            <div class="row">
                                <div class="col-6">
                                    <label for="">WhatsApp</label>
                                    <div class="bor8 m-b-20 how-pos4-parent w-100">
                                        <input class="stext-111 cl2 plh3 size-126 p-l-62 p-r-30" id="no_telp_edit" type="number" name="no_telp" placeholder="Your Number Phone" value="{{ Auth::user()->no_telp }}">
                                        <i class="how-pos4 pointer-none fa fa-whatsapp"></i>
                                    </div>
                                    <p class="text-danger m-b-20" style="margin-top: -20px;" id="no_telp_e"></p>
                                </div>
                                <div class="col-6">
                                    Jenis Kelamin
                                    <div class="bor8 m-b-20 how-pos4-parent w-100">
                                        <select class="stext-111 cl2 plh3 size-126 p-l-62 p-r-30 bor8" id="jenis_kelamin" name="jenis_kelamin">
                                            <option value="" selected disabled>Pilih</option>
                                            <option data-value="Laki-laki" {{ Auth::user()->jenis_kelamin == 'Laki-laki' ? 'selected' : null }}>Laki-laki</option>
                                            <option data-value="Perempuan" {{ Auth::user()->jenis_kelamin == 'Perempuan' ? 'selected' : null }}>Perempuan</option>
                                            <option data-value="Not Found" {{ Auth::user()->jenis_kelamin == 'Not Found' ? 'selected' : null }}>Not Found</option>
                                        </select>
                                        <i class="how-pos4 pointer-none fa fa-transgender"></i>
                                    </div>
                                </div>
                            </div>
                            <label for="">Alamat</label>
                            <div class="bor8 m-b-20 how-pos4-parent w-100">
                                <input class="stext-111 cl2 plh3 size-126 p-l-62 p-r-30" id="alamat_edit" type="text" name="alamat" placeholder="Your Location Address" value="{{ Auth::user()->alamat }}">
                                <i class="how-pos4 pointer-none fa fa-map-marker"></i>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label for="">Rt</label>
                                    <div class="bor8 m-b-20 how-pos4-parent w-100">
                                        <input class="stext-111 cl2 plh3 size-126 p-l-62 p-r-30" id="rt_edit" type="number" name="rt" placeholder="Your Location Address" value="{{ Auth::user()->rt }}">
                                        <i class="how-pos4 pointer-none fa fa-map-marker"></i>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label for="">Rw</label>
                                    <div class="bor8 m-b-20 how-pos4-parent w-100">
                                        <input class="stext-111 cl2 plh3 size-126 p-l-62 p-r-30" id="rw_edit" type="number" name="rw" placeholder="Your Location Address" value="{{ Auth::user()->rw }}">
                                        <i class="how-pos4 pointer-none fa fa-map-marker"></i>
                                    </div>
                                </div>
                            </div>
                            <label for="">Kota</label>
                            <div class="bor8 m-b-20 how-pos4-parent w-100">
                                <input class="stext-111 cl2 plh3 size-126 p-l-62 p-r-30" id="kota_edit" type="text" name="kota" placeholder="Your Location Address" value="{{ Auth::user()->kota }}">
                                <i class="how-pos4 pointer-none fa fa-map-marker"></i>
                            </div>
                            <button class="savedit">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>