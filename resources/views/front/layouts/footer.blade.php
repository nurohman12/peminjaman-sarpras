<!-- Footer -->
<footer class="bg3 p-t-75 p-b-32">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Ketegori
                </h4>

                <ul>
                    <li class="p-b-10">
                        <a href="#!" class="stext-107 cl7 hov-cl1 trans-04">
                            Mebel
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#!" class="stext-107 cl7 hov-cl1 trans-04">
                            Elektronik
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#!" class="stext-107 cl7 hov-cl1 trans-04">
                            Ruangan
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#!" class="stext-107 cl7 hov-cl1 trans-04">
                            Lainnya
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Bantuan
                </h4>

                <ul>
                    <li class="p-b-10">
                        <a href="#!" class="stext-107 cl7 hov-cl1 trans-04">
                            Alur Peminjaman
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#!" class="stext-107 cl7 hov-cl1 trans-04">
                            Pengembalian
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#!" class="stext-107 cl7 hov-cl1 trans-04">
                            Persiapan
                        </a>
                    </li>

                    <li class="p-b-10">
                        <a href="#!" class="stext-107 cl7 hov-cl1 trans-04">
                            FAQ
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Hubungkan
                </h4>

                <p class="stext-107 cl7 size-201">
                    Ada pertanyaan? Beri tahu kami di kantor di lantai 1, <br> Jl. Lingkar Maskumambang No.1, Sukorame, Kec. Mojoroto, Kota Kediri, Jawa Timur 64119 atau <br>hubungi kami di (+365) 96 716 6879
                </p>

                <div class="p-t-27">
                    <a href="#!" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-facebook"></i>
                    </a>

                    <a href="#!" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-instagram"></i>
                    </a>

                    <a href="#!" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
                        <i class="fa fa-pinterest-p"></i>
                    </a>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3 p-b-50">
                <h4 class="stext-301 cl0 p-b-30">
                    Berita
                </h4>

                <form>
                    <div class="wrap-input1 w-full p-b-4">
                        <input class="input1 bg-none plh1 stext-107 cl7" type="text" name="email" placeholder="email@example.com">
                        <div class="focus-input1 trans-04"></div>
                    </div>

                    <div class="p-t-18">
                        <button class="flex-c-m stext-101 cl0 size-103 bg1 bor1 hov-btn2 p-lr-15 trans-04">
                            Langganan
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="p-t-40">

            <p class="stext-107 cl6 txt-center">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>
                    document.write(new Date().getFullYear());
                </script> All rights reserved | Made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> &amp; distributed by <a href="https://themewagon.com" target="_blank">ThemeWagon</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

            </p>
        </div>
    </div>
</footer>


<!-- Back to top-->
<!-- <div class="btn-back-to-top" id="myBtn">
    <span class="symbol-btn-back-to-top">
        <i class="zmdi zmdi-chevron-up"></i>
    </span>
</div>  -->
<!--===============================================================================================-->
<script src="{{ asset('/front') }}/vendor/jquery/jquery-3.2.1.min.js"></script>

@auth
<script>
    function totalDraf() {
        $.ajax({
            type: 'GET',
            url: '/draft_count',
            success: function(response) {
                var response = JSON.parse(response);
                $('.js-show-cart').attr('data-notify', response);
                $('#total_miniDraf').text("Total : " + response);
                $('#total_draft').text(response);
            }
        })
    }

    $(document).on('click', '.js-show-cart', function() {
        ReadMiniDraft();
    });
    $.get("{{ url('/draft') }}", function() {
        ReadDraft();
    });

    function ReadMiniDraft() {
        $.get("{{ url('/draft/mini') }}", {}, function(data, status) {
            $("#miniDraft").html(data);
            totalDraf();
        });
    }

    function ReadDraft() {
        $.get("{{ url('/draft_') }}", {}, function(data, status) {
            $("#table_draft").html(data);
            totalDraf();
        });
    }

    function mini_draft_destroy(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: "POST",
            url: "/draft/mini/destroy/" + id,
            success: function(data) {
                ReadMiniDraft();
                ReadDraft();
            }
        })
    }

    function draft_destroy(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: "DELETE",
            url: "/draft/" + id,
            success: function(data) {
                ReadMiniDraft();
                ReadDraft();
            }
        })
    }
</script>
@endauth
<!--===============================================================================================-->
<script src="{{ asset('/front') }}/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="{{ asset('/front') }}/vendor/bootstrap/js/popper.js"></script>
<script src="{{ asset('/front') }}/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="{{ asset('/front') }}/vendor/select2/select2.min.js"></script>
<script>
    $(".js-select2").each(function() {
        $(this).select2({
            minimumResultsForSearch: 20,
            dropdownParent: $(this).next('.dropDownSelect2')
        });
    })
</script>
@stack('script')
<!--===============================================================================================-->
<script src="{{ asset('/front') }}/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script>
    $('.js-pscroll').each(function() {
        $(this).css('position', 'relative');
        $(this).css('overflow', 'hidden');
        var ps = new PerfectScrollbar(this, {
            wheelSpeed: 1,
            scrollingThreshold: 1000,
            wheelPropagation: false,
        });

        $(window).on('resize', function() {
            ps.update();
        })
    });
</script>
<!--===============================================================================================-->
<script src="{{ asset('/front') }}/js/main.js"></script>

</body>

</html>