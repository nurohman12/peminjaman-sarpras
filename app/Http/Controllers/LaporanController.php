<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function ketersediaan()
    {
        return view('back.laporan.ketersediaan');
    }
    public function kerusakan()
    {
        return view('back.laporan.kerusakan');
    }
}
